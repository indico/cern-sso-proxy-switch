#!/bin/sh

if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="${NAMESPACE}.web.cern.ch"
fi

# Replace hostname in shibboleth2.xml template
sed "s/example.com/$HOSTNAME_FQDN/" /shibboleth2.xml > /etc/shibboleth/shibboleth2.xml

# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /etc/shibboleth
if [ -n "$(ls -A /tmp/configmap)" ]
then
  for f in /tmp/configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/configmap/* /etc/shibboleth/
    fi
  done
fi

exec /usr/sbin/shibd -F -c /etc/shibboleth/shibboleth2.xml
