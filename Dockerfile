FROM cern/cc7-base

RUN yum update -y && yum install -y httpd shibboleth log4shib xmltooling-schemas opensaml-schemas libcurl-openssl mod_auth_kerb mod_ldap mod_ssl gettext strace && \
    yum clean all

# Shibboleth cannot use system libcurl on centos cf. https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPLinuxRH6
ENV LD_PRELOAD=/opt/shibboleth/lib64/libcurl.so.4

# Prepare folders and make sure permissions allow running httpd/shibd as random unprivileged user ID.
# /var/rub/shibboleth will be an EmptyDir where both containers can write
RUN mkdir -p /usr/local/httpd/ && chmod a+rw /usr/local/httpd && \
    mkdir -p /var/run/shibboleth/ && chmod a+rw /var/run/shibboleth && \
    mkdir -p /etc/httpd/conf.d/configurable/ && chmod -R a+rw /etc/httpd/conf.d/ && chmod a+rw /etc/httpd/conf/httpd.conf && \
    chmod a+rw /etc/shibboleth/ && touch /etc/shibboleth/shibboleth2.xml && chmod a+rw /etc/shibboleth/shibboleth2.xml && \
    mkdir -p /run/httpd/ && chown -R root:root /run/httpd && chmod a+rw /run/httpd

COPY shib_httpd.conf /etc/shibboleth/
COPY httpd.conf /etc/httpd/conf/httpd.conf

# get updated config files from SWITCH
RUN curl -k 'https://www.switch.ch/aai/docs/shibboleth/SWITCH/2.6/sp/deployment/download/customize.php/shibboleth2.xml?osType=nonwindows&hostname=example.com&targetURL=https%3A%2F%2Fexample.com&keyPath=%2Fetc%2Fshibboleth%2Fsp-key.pem&certPath=%2Fetc%2Fshibboleth%2Fsp-cert.pem&federation=SWITCHaai&supportEmail=indico-team%40cern.ch&wayfURL=https%3A%2F%2Fwayf.switch.ch%2FSWITCHaai%2FWAYF&metadataURL=http%3A%2F%2Fmetadata.aai.switch.ch%2Fmetadata.switchaai%2Bidp.xml&metadataFile=metadata.switchaai%2Bidp.xml&hide=windows-only,toolbox,' \
         | sed 's/example.com/$HOSTNAME_FQDN/' > /shibboleth2.xml && \
    curl -k -o /etc/shibboleth/attribute-map.xml 'https://www.switch.ch/aai/docs/shibboleth/SWITCH/2.6/sp/deployment/download/customize.php/attribute-map.xml?osType=nonwindows&hide=' && \
    curl -k -o /etc/shibboleth/attribute-policy.xml 'https://www.switch.ch/aai/docs/shibboleth/SWITCH/2.6/sp/deployment/download/customize.php/attribute-policy.xml?osType=nonwindows&hide=toolbox,'

# download and check root certificate
RUN curl -o /etc/shibboleth/SWITCHaaiRootCA.crt.pem http://ca.aai.switch.ch/SWITCHaaiRootCA.crt.pem
RUN test `openssl x509 -in /etc/shibboleth/SWITCHaaiRootCA.crt.pem -noout -fingerprint -sha256 | cut -c20-` = '37:DC:E4:D7:1C:24:42:32:6A:0F:85:B6:12:00:22:C7:54:AA:FF:B2:8C:BF:CF:69:EB:F3:F7:31:90:3C:09:5A'

# Copy entrypoint files, shibboleth config template and redirect shibboleth logs to stdout
COPY httpd.sh shib.sh /
RUN chmod +x /httpd.sh && chmod +x /shib.sh && chmod a+rw /shibboleth2.xml && chmod a+rw /var/cache/shibboleth/ && rm /etc/shibboleth/sp-{key,cert}.pem && \
    chown root:root /var/log/shibboleth && chmod -R a+rw /var/log/shibboleth && ln -sf /dev/stdout /var/log/shibboleth/shibd.log

RUN chmod -R a+rw /etc/shibboleth

# entrypoint will be overriden for the shibboleth container
ENTRYPOINT ["/httpd.sh"]
EXPOSE 8081
