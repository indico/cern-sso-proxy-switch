---
# This template is intended for users in order to protect a website behind a SSO proxy
# By default, all the paths will be behind the proxy, but it is possible for the users
# to set their own config files by modifying the existing ConfigMap and without the need
# of rebuilding a new image.
kind: "Template"
apiVersion: "v1"
metadata:
  name: "cern-sso-proxy"
  annotations:
    description: "Configurable SSO Proxy for an application"
labels:
  template: "cern-sso-proxy" #this label will applied to all objects created from this template
objects:
  - kind: "Service"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
    spec:
      ports:
        - name: "8081-tcp"
          protocol: "TCP"
          port: 8081
          targetPort: 8081
      selector:
        name: "cern-sso-proxy"
      portalIP: ""
      type: "ClusterIP"
      sessionAffinity: "None"
  - kind: "Route"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
      labels:
        # This will take of the route registration
        cern.ch/sso-registration: Shibboleth
    spec:
      to:
        kind: "Service"
        name: "cern-sso-proxy"
      port:
        targetPort: 8081
      tls:
        termination: "edge"
        insecureEdgeTerminationPolicy: Redirect
  -
    kind: "ConfigMap"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
    data:
      # This configMap contains the configurable files a user has to provide to
      # define the proxy and authorization of the server.
      authorize.conf: |2
        # Make sure clients cannot fake authentication by injecting a X-Remote-User header
        RequestHeader unset X-Remote-User

        #Protected resources, if you need to protect specific urls please change the Location path
        <Location "/">
          ShibRequestSetting requireSession 1
          AuthType shibboleth
          <RequireALL>
            Require valid-user
            Require shib-attr ADFS_GROUP ${AUTHORIZED_GROUPS}
          </RequireALL>
          # Make the value of REMOTE_USER (the email address) available to the backend
          # application as HTTP header X-Remote-User
          RequestHeader set X-Remote-User %{REMOTE_USER}e
          # Use the following instead to pass login name rather than email address
          #RequestHeader set X-Remote-User %{ADFS_LOGIN}e
        </Location>
      proxy.conf: |2
        <Location "/">
          ProxyPreserveHost On
          ProxyPass http://${SERVICE_HOST}:${SERVICE_PORT}/
        </Location>
  -
    kind: "ConfigMap"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-shib"
    data:
      # The shibboleth2.xml configmap is empty by default so the one from the image
      # will be taken instead
      shibboleth2.xml: ""
  -
    kind: "DeploymentConfig"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
    spec:
      strategy:
        type: "Rolling"
      triggers:
      - type: "ConfigChange"
      - type: "ImageChange"
        imageChangeParams:
          automatic: true
          containerNames:
          - "shibd"
          from:
            kind: "ImageStreamTag"
            name: "cern-sso-proxy:stable"
            namespace: openshift
      - type: "ImageChange"
        imageChangeParams:
          automatic: true
          containerNames:
          - "httpd"
          from:
            kind: "ImageStreamTag"
            name: "cern-sso-proxy:stable"
            namespace: openshift
      replicas: 1
      selector:
        name: "cern-sso-proxy"
      template:
        metadata:
          labels:
            name: "cern-sso-proxy"
        spec:
          containers:
            -
              name: "httpd"
              image: "cern-sso-proxy:stable"
              readinessProbe:
                failureThreshold: 3
                # use a short period so as not to slow down startup too much
                periodSeconds: 5
                successThreshold: 1
                tcpSocket:
                  port: 8081
                timeoutSeconds: 1
              resources:
                limits:
                  cpu: 250m
                  memory: 100Mi
                requests:
                  cpu: 25m
                  memory: 50Mi
              volumeMounts:
                # Shared mount for communication between both containers
              - mountPath: /var/run/shibboleth
                name: shared
                # Mount with apache configurable files
              - mountPath: /etc/httpd/conf.d/configurable
                name: apache
              - mountPath: /tmp/configmap
                name: shib
              env:
              -
                name: "NAMESPACE"
                valueFrom:
                  fieldRef:
                    apiVersion: v1
                    fieldPath: metadata.namespace
              -
                name: SERVICE_NAME
                value: "${SERVICE_NAME}"
              -
                name: HOSTNAME_FQDN
                value: ""
              terminationMessagePath: "/dev/termination-log"
              imagePullPolicy: "IfNotPresent"
              capabilities: {}
              securityContext:
                capabilities: {}
                privileged: false
            -
              name: "shibd"
              command: # Shibd container has a different entrypoint
              - /shib.sh
              image: "cern-sso-proxy:stable"
              resources:
                limits:
                  cpu: 250m
                  memory: 100Mi
                requests:
                  cpu: 25m
                  memory: 50Mi
              volumeMounts:
                # Shared mount for communication between both containers
              - mountPath: /var/run/shibboleth
                name: shared
              - mountPath: /tmp/configmap
                name: shib
              env:
              -
                name: "NAMESPACE"
                valueFrom:
                  fieldRef:
                    apiVersion: v1
                    fieldPath: metadata.namespace
              -
                name: HOSTNAME_FQDN
                value: ""
              terminationMessagePath: "/dev/termination-log"
              imagePullPolicy: "IfNotPresent"
              capabilities: {}
              securityContext:
                capabilities: {}
                privileged: false
          volumes:
            -
              emptyDir: {}
              name: shared
            - configMap:
                name: cern-sso-proxy
              name: apache
            - configMap:
                name: cern-sso-shib
              name: shib
          restartPolicy: "Always"
          dnsPolicy: "ClusterFirst"
parameters:
  -
    name: AUTHORIZED_GROUPS
    description: "CERN e-group(s) to be authorized to access the service. It can be more than on e-group if separated by spaces"
    value: it-dep
  -
    name: SERVICE_NAME
    description: "Name of the Openshift Service for the backend application. After authentication, requests will be proxied to that service. It can be obtained with 'oc get services' or from the web interface."
    required: true
